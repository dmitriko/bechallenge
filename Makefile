TAG := $(shell git describe)

build-backend:
	docker build . -f Dockerfile.backend -t dmitriko/bechallenge:${TAG}

venv:
	python3 -m venv venv

local-env: venv
	./venv/bin/pip install -U pip
	./venv/bin/pip install -r requirements/local.txt

.PHONY: build-backend local-env
