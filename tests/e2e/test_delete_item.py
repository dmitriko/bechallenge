'''Add an e-2-e test for the delete method only
(let other team members follow your lead and implement the rest)

'''

import os

import psycopg2

from app import models, db


APP_URL =  os.environ["APP_URL"]


def _insert_entry(title, description):
    'Insert Entry using SQLAlchemy'
    entry = models.Entry(title=title, description=description)
    db.session.add(entry)
    db.session.commit()
    return entry.id


def _entry_exists(entry_id):
    'Return true if entry exists'
    try:
        dbname = os.environ['POSTGRES_DB']
        pwd = os.environ['POSTGRES_PASSWORD']
        host = os.environ['POSTGRES_HOST']
        user = os.environ['POSTGRES_USER']
    except KeyError as exc:
        raise RuntimeError("POSTGRES_* vars are not set") from exc

    try:
        conn = psycopg2.connect(user=user,
                                password=pwd,
                                host=host,
                                database=dbname)
        cursor = conn.cursor()
        cursor.execute('select id from entry where id=%s', (entry_id,))
        records = cursor.fetchall()
        return len(records) == 1
    finally:
        if conn:
            cursor.close()
            conn.close()


def test_get_index(browser):
    browser.get(APP_URL)
    assert 'Title' in browser.title


def test_delete_item(browser):
    ''' Add a record via SQLAlchemy,
    use web browser to open a page and click delete link,
    make sure it clicked exactly element added before.
    Check is record deleted via psycopg2

    '''
    entry_id = _insert_entry('foo', 'bar')
    browser.get(APP_URL)
    elements = browser.find_elements_by_link_text('Delete')
    assert len(elements)
    btn_clicked = False
    for element in elements:
        if element.get_attribute('href').endswith('/delete/{}'.format(entry_id)):
            element.click()
            btn_clicked = True
    assert btn_clicked
    assert not _entry_exists(entry_id)
