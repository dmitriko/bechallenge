import pytest
from selenium import webdriver
from selenium.webdriver import Remote


@pytest.fixture(scope='function')
def browser() -> Remote:
    driver = Remote('http://selenium:4444/wd/hub',
                    options=webdriver.ChromeOptions()
            )
    yield driver
    driver.quit()

